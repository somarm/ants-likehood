# Ants likehood APP:

Checklist: 

* Users must be able to tap a button that fetches the ant data 

* When data has been fetched, User must be able to Start a Race, which triggers running calculations on all ants simultaneously.

* When data has been fetched, UI must reflect the state of each ant's win likelihood calculation (not yet run, in progress, calculated, etc.).

* When data has been fetched, UI must display the global state of the race (not yet run, in progress, all calculated).

* When a race is ongoing, as the results come in, the list of ants must be ordered by calculated likelihood of winning
    - highest
    - lowest
    - not yet calculated

## Run locally

### `yarn`
then:
### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


