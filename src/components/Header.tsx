import React from 'react';
interface HeaderProps {
  getAllAnts: any;
}
const Header :React.FC<HeaderProps> = ({getAllAnts}) => {
  return(
    <nav className="navbar navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">Ants Race</a>
        <button className="btn btn-primary" onClick={() => getAllAnts()}>Fetch Ants</button>
      </div>
    </nav>
  )
}


export default Header;
