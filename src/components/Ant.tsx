import React from 'react';
import AntIcon from './../assets/icons/ant.svg'
import AntData from '../types/ant.type';

interface AntProp {
  ant: AntData,
  raceStarted: boolean,
  updateWinLikeHood: any,

}
export default class Ant extends React.Component<AntProp>  {

  constructor(props:AntProp) {
    super(props);
  }

  componentDidUpdate = (prevProps:AntProp) => {
    if (prevProps.raceStarted !== this.props.raceStarted) {
      this.generateAntWinLikelihoodCalculator()(
        (likelihood:number) => { this.props.updateWinLikeHood(this.props.ant.name, { winLikeHoodVal: likelihood, raceDone: true }) }
      )
    }
  }

  generateAntWinLikelihoodCalculator = () => {
    const delay = 7000 + Math.random() * 7000;
    const likelihoodOfAntWinning = Math.random();

    return (callback:any) => {
      setTimeout(() => {
        callback(likelihoodOfAntWinning);
      }, delay);
    };
  }

  renderAntLikeHoodBadge = () => {
    if(this.props.ant.winLikeHoodVal) {
      return(
        <h6>
          Likehood Calculation:
          <span className="badge bg-secondary sm">calculated { this.props.ant.winLikeHoodVal }</span>
        </h6>
      )
    }
    if(this.props.raceStarted) {
      return(
        <h6>
          Calculation status:
          <span className="badge bg-secondary sm">in progress</span>
        </h6>
      )
    }
    return(
      <h6>
        Calculation status:
        <span className="badge bg-secondary sm">not yet run</span>
      </h6>
    )
  }
  render = () => {
    const {name, color} = this.props.ant;

    return(
      <div className="ant-container">
        <div className="ant-color-holder rounded-circle" style={{background: color}} />

        <img src={AntIcon} className={"rounded-circle"} alt={name} width={50} height={50}/>
        <span className="ant-name">{name}</span>
        { this.renderAntLikeHoodBadge() }
      </div>
    )
  }

}
