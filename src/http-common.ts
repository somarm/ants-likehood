import axios from "axios";

export default axios.create({
  baseURL: "https://sg-ants-server.herokuapp.com/",
  headers: {
    "Content-type": "application/json"
  }
});
