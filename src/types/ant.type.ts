export default interface AntData {
  weight: number,
  name: string,
  length: string,
  color: string,
  winLikeHoodVal: number | 0,
  raceDone?: boolean | false
}
