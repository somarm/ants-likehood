import React from 'react';
import AntsDataService from './services/ants.service';
import Header from './components/Header';
import AntData from './types/ant.type';
import Ant from './components/Ant';

type Ants = Array<AntData>;
interface AppProps {
  ants?: Ants | [],
  raceStarted?: boolean|false
}

type initialSatate = {
  ants: Ants | [],
  raceStarted: boolean|false
}
class App extends React.Component<AppProps, initialSatate>  {

  constructor(props:any) {
    super(props);
    this.state = { ants: [], raceStarted: false };
  }

  getAllAntsAndUpdateState = () => {
    AntsDataService.getAll().then(
      (res: any) =>
        this.setState({ants: res.data.ants, raceStarted: false})
      ).catch((err)=> console.log(err))
  }

  startRace = () => {
    this.setState({raceStarted: true})
  }

  updateAnt = (name: string, itemAttributes: Object) => {
    var index = this.state.ants.findIndex((ant:AntData) => ant.name === name);

      this.setState({
        ants: [
           ...this.state.ants.slice(0,index),
           Object.assign({}, this.state.ants[index], itemAttributes),
           ...this.state.ants.slice(index+1)
        ].sort((a, b) =>
          // @ts-ignore
          a.winLikeHoodVal > b.winLikeHoodVal ? -1: 1
        )
      });
      console.log(this.state.ants)
  }

  startRaceButtonRenderer = () => {
    if(this.state.ants.length === 0) {
      return(
        <div>
          <p>Fetch data to start the race</p>
        </div>
      );
    }
    if(this.state.raceStarted){
      return
    }
    return(
      <button onClick={this.startRace} className={"btn btn-success"}>Start Race</button>
    )
  }

  renderRaceState = () => {
    if(this.state.raceStarted === false){
      return <span className="badge rounded-pill bg-primary">not yet run</span>
    }

    if(this.state.raceStarted === true && this.state.ants.every((a) => a.raceDone === true)){
      return <span className="badge rounded-pill bg-primary">All calculated</span>
    }
    return <span className="badge rounded-pill bg-primary">in progress</span>
  }

  render = () => {
    const ants = this.state.ants;

    return(
      <div className="container">
        <Header getAllAnts={this.getAllAntsAndUpdateState} />
        {
          this.renderRaceState()
        }
        {
          this.startRaceButtonRenderer()
        }
        <div>

            {
              ants.map(
                (ant: AntData, index:number) => <Ant updateWinLikeHood={this.updateAnt} key={index} raceStarted={this.state.raceStarted}  ant={ant} />)
            }

        </div>
      </div>
    );
  }
}

export default App;
