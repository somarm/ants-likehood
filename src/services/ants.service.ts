import http from '../http-common';

class AntsDataService {
  getAll() {
    return http.get("/ants");
  }
}


export default new AntsDataService();
